package edu.sdsu.cs;

/*Program3
Stefan Carnahan

Brendan Terry
cssc0827
 */


import edu.sdsu.cs.datastructures.DirectedGraph;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class App {


    private static File file;
    private static ArrayList<File> toRead = new ArrayList<File>();

    private static DirectedGraph directedGraph;


    public static void main(String[] args) {
        String dir;


        if (args.length > 0) {
            dir = args[0];
        } else {

            dir = "C:\\Users\\brendan\\IdeaProjects\\program3\\src\\main\\java\\edu\\sdsu\\cs\\layout.csv";
        }
        file = new File(dir);

        directedGraph = new DirectedGraph();

        if (file.canRead()) {
            if (fileFilter(file)) {
                readFile(file);
            }
        } else {
            error();
        }

        print();

        shortest();

    }

    private static Boolean fileFilter(File file) {
        FilenameFilter extensionFilter = new FilenameFilter() {
            public boolean accept(File file, String name) {
                String fileName = name.toLowerCase();
                if (fileName.endsWith(".csv")) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        if (extensionFilter.accept(file, file.toString())) {
            return true;
        } else {
            return false;
        }
    }

    private static void readFile(File file) {

        try {
            BufferedReader read = new BufferedReader(new FileReader(file));

            Scanner scanner = new Scanner(read);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] places = line.split(",");


                if (places.length == 1) {
                    if (!(directedGraph.contains(places[0]))) {
                        directedGraph.add(places[0]);
                    }
                }
                if (places.length == 2) {
                    if (!(directedGraph.contains(places[0]))) {
                        directedGraph.add(places[0]);
                    }
                    if (!(directedGraph.contains(places[1]))) {
                        directedGraph.add(places[1]);
                    }
                    if (!(directedGraph.isConnected(places[0], places[1]))) {
                        directedGraph.connect(places[0], places[1]);
                    }
                }
            }
        } catch (IOException fileError) {
            error();
        }
    }

    private static void error() {
        System.out.println("Error: Unable to open filename. Verify the file exists. \n");
    }

    private static void shortest() {
        String[] place = {"", ""};

        Scanner input = new Scanner(System.in);
        System.out.println("Enter a starting point: ");
        place[0] = input.next();
        System.out.println("Enter a ending point: ");
        place[1] = input.next();
        System.out.print(directedGraph.shortestPath(place[0], place[1]));
    }

    private static void print() {
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.equals(directedGraph.vertices());
        int i = 0;
        while (i < linkedList.size()) {
            System.out.println(linkedList.get(i));
            System.out.print(directedGraph.neighbors(linkedList.get(i)));
            i++;
            System.out.print("********************************************************************************");
        }
    }

}